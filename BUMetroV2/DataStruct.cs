﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUMetroV2
{
    public struct Group
    {
        public string name { get; set; }
        public List<ForumsList> forumslist { get; set; }


    }

    public struct ForumsList
    {
        public string name { get; set; }


        //public List<SubForumsList> subforumslist { get; set; }

    }

    public class SubForumsList
    {
        public string name { get; set; }


    }
}
