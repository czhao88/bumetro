﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using System.Net.Http;



// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BUMetroV2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 

    
    public class FeedDataSource
    {
        public int test;
    }

    public sealed partial class MainPage : Page
    {

        static string UserName = null;
        static string Password = null;
        

        
        static GetForumList getForumList = new GetForumList();
        UserInfo userInfo = null;
        
        public MainPage()
        {
            this.InitializeComponent();
            
            
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            
            this.ItemListView.DataContext = getForumList.userInfo;
            this.btnUpdate.IsEnabled = false;
            
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
 

            

            string userName = UserName;
            string password = Password;

            string loginUrl = "http://out.bitunion.org/open_api/bu_logging.php";
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("action", "login");
            parameters.Add("username", userName);
            parameters.Add("password", password);
            
            //HttpWebResponseUtility.CreatePostHttpResponse(loginUrl, parameters, null, null, Encoding.UTF8, null);

            //WebResponse response = HttpWebResponseUtility.result;


            getForumList.action = GetForumList.Action.login;
            lbState.DataContext = userInfo;
            getForumList.MakeAsyncRequest(loginUrl, parameters, Encoding.UTF8);

            userInfo = getForumList.userInfo;
            if (userInfo.session != null)
            {
                this.btnUpdate.IsEnabled = true;
            }
            ;

        }

       

        
        private void tbUsername_TextChanged(object sender, TextChangedEventArgs e)
        {
            UserName = this.tbUsername.Text;
        }

        private void pwBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Password = this.pwBox.Password;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("action", "forum");
            parameters.Add("username", getForumList.userInfo.username);
            parameters.Add("session", getForumList.userInfo.session);

            getForumList.action = GetForumList.Action.getforum;

            string loginUrl = "http://out.bitunion.org/open_api/bu_forum.php";
            
            getForumList.MakeAsyncRequest(loginUrl, parameters, Encoding.UTF8);
            

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //string test = @"{""username"":""test"",""age"":{""10"":""3b""}}";

            //JObject jObj = JObject.Parse(test);
           

            //Test result = Newtonsoft.Json.JsonConvert.DeserializeObject<Test>(test);
            ////Dictionary<string, string> result = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(test);
            //result = result;

            //string forumname = "%E7%B3%BB%E7%BB%9F%E7%AE%A1%E7%90%86%E5%8C%BA";

        }

        public class Test
        {
            public string username { get; set; }
            public Dictionary<String, Age> age { get; set; }
        }

        public class Age
        {
            public Dictionary<String, String> age { get; set; }
        }


    }

}