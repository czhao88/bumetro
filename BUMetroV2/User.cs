﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUMetroV2
{
    public class UserInfo
    {
        public string result
        {
            get;
            set;
        }
        public int uid { get; set; }
        public string username { get; set; }
        public string session { get; set; }
        public string status { get; set; }
        public int credit { get; set; }
        public string lastactivity { get; set; }

    }//注:ScriptIgnore特性用来标记不需要被序列化的属性

    
    class User
    {
    }
}
