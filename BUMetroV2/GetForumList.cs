﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace BUMetroV2
{
 

    public class GetForumList
    {

        public enum Action { login, getforum };
        public string postData = null;
        public string session = null;
        
        public Action action = new Action();

        public List<Group> grouplist = new List<Group>();
        public UserInfo userInfo = new UserInfo();
        public string responseString = null;

        public void MakeAsyncRequest(string url, IDictionary<string, string> parameters, Encoding requestEncoding)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "Post";
            request.Proxy = null;
            request.ContentType = "application/x-www-form-urlencoded";



            //If POST  
            if (!(parameters == null || parameters.Count == 0))
            {
                StringBuilder buffer = new StringBuilder();
                int i = 0;
                foreach (string key in parameters.Keys)
                {
                    if (i > 0)
                    {
                        buffer.AppendFormat(",\"{0}\":\"{1}\"", key, parameters[key]);
                    }
                    else
                    {
                        buffer.Append("{");
                        buffer.AppendFormat("\"{0}\":\"{1}\"", key, parameters[key]);
                    }
                    i++;
                }
                buffer.Append("}");
                byte[] data = requestEncoding.GetBytes(buffer.ToString());

                postData = buffer.ToString();

                request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);


            }

        }

        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

            // End the operation
            Stream postStream = request.EndGetRequestStream(asynchronousResult);


            // Convert the string into a byte array. 
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Write to the request stream.
            postStream.Write(byteArray, 0, postData.Length);
            //postStream.Close();

            // Start the asynchronous operation to get the response
            request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
        }

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

            // End the operation
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
            Stream streamResponse = response.GetResponseStream();
            StreamReader streamRead = new StreamReader(streamResponse);
            responseString = streamRead.ReadToEnd();
            JsonToStruct(responseString);

        }

        public void JsonToStruct(string json)
        {
            if (action == Action.login)
            {
                userInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfo>(json);
            }

            if (action == Action.getforum)
            {


                JObject jObj = JObject.Parse(responseString);
                //JObject jObj2 = JObject.Parse(jObj.Last.ToString());
                //JObject jObj1 = JObject.Parse(jObj.Children<>)
                string temp = jObj["forumslist"].ToString();
                JObject jObj2 = JObject.Parse(temp);

                IList<string> keys = jObj2.Properties().Select(p => p.Name).ToList();

                string[] groupname = new string[keys.Count];
                String[] mainforumjson = new string[keys.Count];
                var forumgroups = jObj2.Children();
                Group group = new Group();


                for (int i = 0; i < keys.Count - 1; i++)
                {
                    groupname[i] = jObj2[keys[i]]["main"]["name"].ToString();
                    groupname[i] = System.Uri.UnescapeDataString(groupname[i]);


                    group.name = groupname[i];


                    mainforumjson[i] = jObj2[keys[i]].ToString();//jObj2[keys[i]]["main"]["name"].ToString();
                    IList<string> forumkeys = JObject.Parse(mainforumjson[i]).Properties().Select(p => p.Name).ToList();

                    ForumsList forum = new ForumsList();
                    group.forumslist = new List<ForumsList>();
                    for (int j = 1; j < forumkeys.Count; j++)
                    {
                        JObject mainforum = JObject.Parse(JObject.Parse(mainforumjson[i])[forumkeys[j]].ToString());
                        JArray forums = JArray.Parse(mainforum["main"].ToString());

                        string forumname = forums[0]["name"].ToString();
                        forumname = System.Uri.UnescapeDataString(forumname);

                        forum.name = forumname;

                        group.forumslist.Add(forum);
                        ;
                    }

                    grouplist.Add(group);
                }



                //JObject new1 = JObject.Parse(forumgroups[keys[0]].ToString());



                temp = jObj2["5"].ToString();
                JObject jObj3 = JObject.Parse(temp);
                //string forumname = jObj3["main"]["name"].ToString();
                //forumname = System.Uri.UnescapeDataString(forumname);


                    responseString = responseString;


            }
        }
        

        
    }
}
